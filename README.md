# Starter project Firebasee

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.3.

## Setup

* Clone or download this repository
* Create a project on firebase.com
* Add a collection `messages` to the Cloud Firestore with a document that has a `text` property of type `string`
* Replace keys in `environments.ts` with your own project keys.
* Run `npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
